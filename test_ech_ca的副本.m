function [ech,ca] = test_ech_ca(A)
    [m,n] = size(A);
    ech = 1; %initial value, if there exists any violation, it becomes 0
    ca = 1;
    for i = 1:m%the row of the matrix
        for j = 1:n%the colunm of the matrix
            if(A(i,j)~=0)
                for k = (i+1):m
                    if(A(k,j)~=0)
                        ech = 0;
                        break
                    end
                end
                if(i < m)
                    i = i+1;%continue to check the next row if it is not the last row
                end
            end
            if(ech == 0)%it is not in echelon form
                break
            end 
        end
        if(ech == 0)
            break
        end
    end
    
    if(ech == 0)
        ca = 0;%if not in echelon form then not in canonical form
    else
        for i = 1:m
            for j = 1:n
                if(A(i,j)~=0)
                    if(A(i,j)~=1)
                        ca = 0;
                        break
                    end
                    for k = 1:(i-1)
                        if(A(k,j)~=0)
                            ca = 0;
                            break
                        end
                    end
                    if(i < m)
                        i = i+1;%continue to check the next row if it is not the last
                    end
                end
                
            end
            
            if(ca == 0)
                break
            end
        end
    end
     %if the value remain 1 after all the tests, then ech or ca is true 
ech
ca
end