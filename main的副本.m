m = 10:10:100;
n = 10:10:100;
for i = 1:10
    for j = 1:10
        A = ones(m(i),n(j)-1);
        b = ones(m(i),1);
        ticsurf
        x = mat_vec_x(A,b);
        time(i,j) = toc;
    end
end

surface(m,n,time)