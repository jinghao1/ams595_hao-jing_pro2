function x = mat_vec_x(A,b)
    %if there exists multiple solutions, this function returns
    %all the free variables to be 0

    [m,n] = size(A);
    [m1,n1] = size(b);
    
    assert(n1==1,'b is not a nx1 vector');
    assert(m == m1,'the dimension of A and b do not match');
    
    %Augmented matrix
    M = zeros(m,n+1);
    M(:,1:n) = A;
    M(:,n+1) = b;
    
    %initialize x
    x = zeros(n,1);
    
    U = returnEch(M);
    C = returnCa(U);
    
    for i = 1:m
        nonzero_row = 1;
        for j = 1:n
            
            if(C(i,j)~=0)
                nonzero_row = 0;
                x(j) = C(i,n+1)/C(i,j);
            end
            
        end
        assert(~(nonzero_row&&C(i,n+1)~=0),'no solution')
    end
end