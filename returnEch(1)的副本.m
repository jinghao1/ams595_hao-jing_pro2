function U = returnEch(A)%set up a function which returns the row echelon form for any input matrix A
    [m,n] = size(A);%if A is already in the row_echelon form then return it directly
    [ech,ca] = test_ech_ca(A);%use the function in question(a) to tell an echelon form in step6
    if(ech == 1)
        U = A;
        return 
    end
    for i = 1:m
        for j = 1:n 
           if(A(i,j)==0)%step3
               for k = i:m %look for a row"k"
                   nonzero_flag = 0;
                   if(A(k,j)~=0)
                       nonzero_flag = 1;
                       %disp('exchange');
                       %k
                       %i
                       A([i k],:) = A([k i],:);%exchange ith and k-th row
                       break
                   end   
               end
               %step2
               if(nonzero_flag == 0)
                   continue%if all j-th column is zero, continue the loop(j = j+1)
               end
           end
           
           %test if i-th row is the last nonzero row by testing if A is in
           %echelon form
           %step6
           [ech,ca] = test_ech_ca(A);
           if(ech==1)
               
               %if i-th row is already the last nonzero row, return A
               U = A;
               return 
           end
       
           %if there exists non-zero entry in j-th row, do pivoting after
           %exchanging rows
           %step5
           for k2 = (i+1):m
               %k2
               coef = A(k2,j)/A(i,j);
               A(k2,:) = A(k2,:) - coef*A(i,:);
           end
           
           if(i<m)
               i = i+1;%continue to check the next row if it is not the last
           end                
        end    
    end
    
    U = A;  
end