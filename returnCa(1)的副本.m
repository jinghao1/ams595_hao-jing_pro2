assertfunction C = returnCa(U)
    %U should be in echelon form
    %return a row canonical form
    [ech,ca] = test_ech_ca(U);
    assert(ech==1,'the input matrix is NOT in echelon form');
    [m,n] = size(U);
    for i = m:-1:1
        for j = 1:n
            if(U(i,j)~=0)
    %"i"as the last non-zero row,and "j"as the first non-zero entry in this row
            U(i,:) = U(i,:)/U(i,j);
    %covert the pivot entry to "1"by multiplying R_i by(1/a_ij)
                for k<i
                    U(k,:) = U(k,:) - U(k,j)*U(i,:);
                end
                if(i<m)
                    i = i+1;%continue to check the last row if it is not the first
                end
    %no need to check the entries to the right if it is the first row
                if(i == m)
                    break
                end
            end
        end
    end
    
    C = U;
    
end